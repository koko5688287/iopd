import Vue from "vue";
import Router from "vue-router";
import Layout from "@/components/Layout/Layout";
import QueueDetail from "@/components/QueueDetail";
import ReQueue from "@/components/ReQueue";
import Appointment from "@/components/Appointment";
import CallPatient from "@/components/CallPatient";
import Communication from "@/components/Communication";
import Location from "@/components/Location";
import Setting from "@/components/Setting";
import Recommend from "@/components/Recommend";
import Export from "@/components/Export";
import Display from "@/components/Display";
import Display_1 from "@/components/Display_1";
import Display_2 from "@/components/Display_2";
import Display_4 from "@/components/Display_4";
import Display_5 from "@/components/Display_5";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      redirect: "/requeue",
      name: "Layout",
      component: Layout,
      children: [
        {
          path: "queue/:queueId/:queueTitle",
          name: "QueueDetail",
          component: QueueDetail,
          meta: {
            title: "QueueDetail"
          }
        },
        {
          path: "export",
          name: "Export",
          component: Export,
          meta: {
            title: "Import/Export"
          }
        },
        {
          path: "requeue",
          name: "ReQueue",
          component: ReQueue,
          meta: {
            title: "Overall Queue"
          }
        },
        {
          path: "appointment",
          name: "Appointment",
          component: Appointment,
          meta: {
            title: "Appointment"
          }
        },
        {
          path: "callpatient",
          name: "CallPatient",
          component: CallPatient,
          meta: {
            title: "Call Patient"
          }
        },
        {
          path: "communication",
          name: "Communication",
          component: Communication,
          meta: {
            title: "Organization Communication"
          }
        },
        {
          path: "display",
          name: "Display",
          component: Display,
          meta: {
            title: "Display"
          }
        },
        {
          path: "display_1",
          name: "Display_1",
          component: Display_1,
          meta: {
            title: "Display_1"
          }
        },
        {
          path: "display_2",
          name: "Display_2",
          component: Display_2,
          meta: {
            title: "Display_2"
          }
        },
        {
          path: "display_4",
          name: "Display_4",
          component: Display_4,
          meta: {
            title: "Display_4"
          }
        },
        {
          path: "display_5",
          name: "Display_5",
          component: Display_5,
          meta: {
            title: "Display_5"
          }
        },
        { 
          path: "location",
          name: "Location Settings",
          component: Location,
          meta: {
                title: "Location Settings",
              }
            },
            
        { path: "setting",
          name: "Setting",
          component: Setting,
          meta: {
                title: "Settings",
              }
            },
        { path: "recommend",
          name: "Recommend Settings",
          component: Recommend,
          meta: {
                title: "Recommend Settings",
              }
            }
              ]
             
          }
      ]
});
